// Constructor Functions
const Person = function(firstName, birthYear) {
  // Instance properties
  this.firstName = firstName;
  this.birthYear = birthYear;
};
const ahmad = new Person('Ahmad', 1991);
console.log(ahmad);

const nima = new Person('nima', 2017);
const behnam = new Person('behnam', 1975);

Person.hey = function() {
  console.log('Hey there ');
  console.log(this);
};
Person.hey();
///////////////////////////////////////
// Prototypes
console.log(Person.prototype);
Person.prototype.calcAge = function() {
  console.log(2037 - this.birthYear);
};
ahmad.calcAge();
nima.calcAge();
