// Sets
const ordersSet = new Set([
  'Pasta',
  'Pizza',
  'Pizza',
  'Rice',
  'Pasta',
  'Pizza'
]);
console.log(ordersSet); // {"Pasta", "Pizza", "Rice"}
console.log(new Set('ahmad')); // {"a", "h", "m", "d"}
console.log(ordersSet.size); //3
console.log(ordersSet.has('Pizza')); // true
console.log(ordersSet.has('Bread')); // false
ordersSet.add('Garlic Bread');
ordersSet.add('Garlic Bread');
ordersSet.delete('Rice');
// ordersSet.clear();
console.log(ordersSet);
for (const order of ordersSet) console.log(order); // Pasta  Pizza  Garlic Bread
// Example
const staff = ['Waiter', 'Chef', 'Waiter', 'Manager', 'Chef', 'Waiter'];
const staffUnique = [...new Set(staff)];
console.log(staffUnique); // ["Waiter", "Chef", "Manager"]
console.log(
  new Set(['Waiter', 'Chef', 'Waiter', 'Manager', 'Chef', 'Waiter']).size
); // 3
console.log(new Set('ahmadgholamnia').size); // 9

//---------------------------------------//
//---------------------------------------//
//---------------------------------------//

// Maps: Fundamentals
const rest = new Map();
rest.set('name', 'Ahmad gholamnia');
rest.set(1, 'Kian Tavasoli');
console.log(rest.set(2, 'Tehran, Shahriar'));
rest
  .set('categories', ['a', 'b', 'c', 'd'])
  .set('open', 11)
  .set('close', 23)
  .set(true, 'We are open :D')
  .set(false, 'We are closed :(');
console.log(rest.get('name'));
console.log(rest.get(true));
console.log(rest.get(1));
const time = 8;
console.log(rest.get(time > rest.get('open') && time < rest.get('close')));
console.log(rest.has('categories'));
rest.delete(2);
// rest.clear();
const arr = [1, 2];
rest.set(arr, 'Test');

console.log(rest);
console.log(rest.size);
console.log(rest.get(arr));
